# recepie-app-api-proxy

NGINX proxy app for our recepie app API

## Usage

### Environment variables

* `LISTEN_PORT` - Port to listen on (default `8080`)
* `APP_HOST` - Hostname of the app to forward request to (default `app`)
* `APP_PORT` - Port of the app to forward request to (default `9000`)

